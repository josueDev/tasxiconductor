//
//  ProfileDriverViewController.swift
//  TASXI
//
//  Created by Josué :D on 24/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD

class ProfileDriverViewController: UIViewController , UITableViewDelegate, UITableViewDataSource  {
    
    
    
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var nameDriver: UILabel!
    
    @IBOutlet weak var telephoneDriver: UIButton!
    
    
    
    @IBOutlet var estoyEnCamino: UIButton!
    
    @IBOutlet var yaEstoyCerca: UIButton!
    
    @IBOutlet var yaLlegue: UIButton!
    var tipoMensaje = 0
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "PERFIL DE USUARIO"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data : ObjectDataTravel = ObjectDataTravel.getData().object(at: 0) as! ObjectDataTravel
        
        let nombre_pasajero = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre_pasajero)!)
        let tel_pasajero = String(describing: UserDefaults.standard.value(forKey: DataPersistent.tel_pasajero)!)
        
        nameDriver.text = nombre_pasajero
        
        //stars.setTitle(data.promedio_conductor, for: UIControlState.normal)
        telephoneDriver.setTitle(tel_pasajero, for: UIControl.State.normal)
        telephoneDriver.addTarget(self, action: #selector(self.callDriver), for: UIControl.Event.touchUpInside)
        
        estoyEnCamino.addTarget(self, action: #selector(self.boton1), for: UIControl.Event.touchUpInside)
        
        yaEstoyCerca.addTarget(self, action: #selector(self.boton2), for: UIControl.Event.touchUpInside)
        
        yaLlegue.addTarget(self, action: #selector(self.boton3), for: UIControl.Event.touchUpInside)
        

        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        
        if (id == "0")
        {
            estoyEnCamino.isHidden = true
            yaEstoyCerca.isHidden = true
            yaLlegue.isHidden = true
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func boton1()
    {
        tipoMensaje = 1
        alertaDePasajeCerca()
    }
    
    @objc func boton2()
    {
        tipoMensaje = 2
        alertaDePasajeCerca()
    }
    
    @objc func boton3()
    {
        tipoMensaje = 3
        alertaDePasajeCerca()
    }
    
    @objc func callDriver()
    {
        let data : ObjectDataTravel = ObjectDataTravel.getData().object(at: 0) as! ObjectDataTravel
        if (data.tel_conductor != "")
        {
            if let url = URL(string: "tel://\(data.tel_conductor)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjectComent.getData().count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCellComment
        
        
        let object : ObjectComent = ObjectComent.getData().object(at: indexPath.row) as! ObjectComent
        
        cell.labelText.text = object.comentario_a_conductor
        
        //cell.label.text = text

        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true);
        print(indexPath.row)
        
        
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let pushController = storyboard.instantiateViewController(withIdentifier: "detalle") as! Detalle
        //        self.navigationController?.pushViewController(pushController, animated: true)
        //
        
    }
    

    func getComments()
    {
        HUD.show(.progress)
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        let parameters: Parameters = [
            "id_pasajero": id,
            ]
        
        print("WS_GET_HISTORY_TRAVEL")
        print("\(ApiDefinition.WS_GET_HISTORY_TRAVEL)?id_pasajero=\(id)")
        
        Alamofire.request(ApiDefinition.WS_GET_HISTORY_TRAVEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_GET_HISTORY_TRAVEL: \(json)")
                let resultado = json["resultado"]
                let res = resultado[0]["resultado"]
                if res != 0
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                        
                        ObjectComent.getData().removeAllObjects()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let viaje = json["viaje"]
                        
                        for index in 0..<(Int(viaje.array!.count)) {
                            let id = String(describing:viaje[index]["id"])
                            let id_pasajero = String(describing:viaje[index]["id_pasajero"])
                            let id_conductor = String(describing:viaje[index]["id_conductor"])
                            let nombre_pasajero = String(describing:viaje[index]["nombre_pasajero"])
                            let nombre_conductor = String(describing:viaje[index]["nombre_conductor"])
                            let foto_conductor = String(describing:viaje[index]["foto_conductor"])
                            let unidad = String(describing:viaje[index]["unidad"])
                            let modelo = String(describing:viaje[index]["modelo"])
                            let placas = String(describing:viaje[index]["placas"])
                            let fecha_solicitud = String(describing:viaje[index]["fecha_solicitud"])
                            let fecha_inicio = String(describing:viaje[index]["fecha_inicio"])
                            let fecha_llegada = String(describing:viaje[index]["fecha_llegada"])
                            let lugar_partida = String(describing:viaje[index]["lugar_partida"])
                            let lugar_llegada = String(describing:viaje[index]["lugar_llegada"])
                            let calificacion_conductor = String(describing:viaje[index]["calificacion_conductor"])
                            let comentario_a_conductor = String(describing:viaje[index]["comentario_a_conductor"])
                            let calificacion_pasajero = String(describing:viaje[index]["calificacion_pasajero"])
                            let comentario_a_pasajero = String(describing:viaje[index]["comentario_a_pasajero"])
                            let estatus = String(describing:viaje[index]["estatus"])
                            
                            
                            let object = ObjectComent(id: id, id_pasajero: id_pasajero, id_conductor: id_conductor, nombre_pasajero: nombre_pasajero, nombre_conductor: nombre_conductor, foto_conductor: foto_conductor, unidad: unidad, modelo: modelo, placas: placas, fecha_solicitud: fecha_solicitud, fecha_inicio: fecha_inicio, fecha_llegada: fecha_llegada, lugar_partida: lugar_partida, lugar_llegada: lugar_llegada, calificacion_conductor: calificacion_conductor, comentario_a_conductor: comentario_a_conductor, calificacion_pasajero: calificacion_pasajero, comentario_a_pasajero: comentario_a_pasajero, estatus: estatus)
                            
                            print(id)
                            
                            if ((comentario_a_conductor == "") || (comentario_a_conductor == "null"))
                            {
                                
                            }
                            else{
                                ObjectComent.getData().add(object)
                            }
                            
                            
                        }
                        
                    
                        
                        
                    }
                }
                else
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "Taxsi", message: "No se ha podido cargar los comentarios", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print(error)
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    
                    let alert = UIAlertController(title: "Taxsi", message: "No se ha podido cargar los comentarios", preferredStyle: .alert)
                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    
    @objc func alertaDePasajeCerca()
    {
        HUD.show(.progress)
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        let idViajePorNotificacion = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        let parameters: Parameters = [
            "id_conductor": id,
            "id_viaje" : idViajePorNotificacion,
            "msj" : tipoMensaje,
        ]
        
        print("WS_ENVIAR_MENSAJE_AUTOMATICO")
        print("\(ApiDefinition.WS_ENVIAR_MENSAJE_AUTOMATICO)?id_conductor=\(id)&id_viaje=\(idViajePorNotificacion)&msj=\(tipoMensaje)")
        
        Alamofire.request(ApiDefinition.WS_ALERTA_PASAJE, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_ALERTA_PASAJE: \(json)")
                let resultado = json["resultado"]
                let res = resultado[0]["resultado"]
                if res != 0
                {
                    HUD.flash(.success, delay: 0.5)
                    self.alertaMensajeEnviado()
                }
                else
                {
                    HUD.flash(.error, delay: 0.5)
                    self.alertaMensajeError()
                }
            case .failure(let _):
                HUD.flash(.error, delay: 0.5)
                self.alertaMensajeError()
            }
        }
    }
    
    func alertaMensajeEnviado()
    {
        let alert = UIAlertController(title: "Taxsi", message: "Mensaje enviado correctamente", preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertaMensajeError()
    {
        let alert = UIAlertController(title: "Taxsi", message: "Ha occurrido un error, por favor intente nuevamente", preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    

}
