//
//  InfoDriverViewController.swift
//  TASXI
//
//  Created by Josué :D on 24/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

var imageDriverGlobal = UIImageView()
var nameDriverGlobal = UILabel()
var carGlobal =  UILabel()
var licensePlateGlobal =  UILabel()
var starsGlobal = UILabel()
var detailGlobal = UIButton()
var cancelTravelGlobal = UIButton()

class InfoDriverViewController: UIViewController {
    
    @IBOutlet var imageDriver: UIImageView!
    @IBOutlet var nameDriver: UILabel!
    @IBOutlet var car: UILabel!
    @IBOutlet var licensePlate: UILabel!
    @IBOutlet var stars: UILabel!
    @IBOutlet var detail: UIButton!
    @IBOutlet var cancelTravel: UIButton!
    @IBOutlet weak var contrainButtonCancelDriver: NSLayoutConstraint!
    
    @IBOutlet var buttonCancelTravel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageDriverGlobal = imageDriver
        nameDriverGlobal = nameDriver
        carGlobal =  car
        licensePlateGlobal =  licensePlate
        starsGlobal = stars
        detailGlobal = detail
        cancelTravelGlobal = cancelTravel
        
        nameDriver.adjustsFontSizeToFitWidth = true
        car.adjustsFontSizeToFitWidth = true
        licensePlate.adjustsFontSizeToFitWidth = true
        stars.adjustsFontSizeToFitWidth = true
        
        detail.addTarget(self, action: #selector(self.pushDetail) , for: UIControl.Event.touchUpInside)
        
        cancelTravel.addTarget(self, action: #selector(self.pushCancelTravel) , for: UIControl.Event.touchUpInside)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func pushDetail()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "ProfileDriverViewController") as! ProfileDriverViewController
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    @objc func pushCancelTravel()
    {
        mapViewControllerGlobal?.cancelarViajeConductor()
    }
    
}
