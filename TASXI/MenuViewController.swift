//
//  MenuViewController.swift
//  TASXI
//
//  Created by Josué :D on 11/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import ImageLoader
import Toucan
import Firebase
import FirebaseMessaging
import PKHUD
import Alamofire
import SwiftyJSON

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var phone: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var tableView: UITableView!
    
    
    @IBOutlet var calificacion: UILabel!
    
    @IBOutlet var experiencia: UILabel!
    
    @IBOutlet var años: UILabel!
    
    
    @IBOutlet var estrellas: UIButton!
    
    
    
    private var data: [String] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "PERFIL DE USUARIO"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menubar.png"), style: .done, target: self, action: #selector(closeMenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calificacion.adjustsFontSizeToFitWidth = true
        experiencia.adjustsFontSizeToFitWidth = true
        años.adjustsFontSizeToFitWidth = true
        
        
        name.adjustsFontSizeToFitWidth = true
        email.adjustsFontSizeToFitWidth = true
        phone.adjustsFontSizeToFitWidth = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.white

        data.append("Historial de viajes")
        data.append("Cambiar contraseña")
        data.append("Cerrar sesión")
        // Do any additional setup after loading the view.
    
         drawImage()
        
        let uName = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        let uPhone = String(describing: UserDefaults.standard.value(forKey: DataPersistent.telefono)!)
        let uLastName = String(describing: UserDefaults.standard.value(forKey: DataPersistent.apellidos)!)
        let uEmail = String(describing: UserDefaults.standard.value(forKey: DataPersistent.email)!)
        
        name.text = uName + " " + uLastName
        phone.text = uPhone
        email.text = uEmail
        
        //let imageDownload = ImageLoader.request(with: urlImage, onCompletion: { _,_,_  in })
        
    }
    
    func drawImage()
    {
        let urlImage = String(describing: UserDefaults.standard.value(forKey: DataPersistent.imageUrl)!)
        
        print("urlImage \(urlImage)")
        let imageTest = UIImageView()
        imageTest.load.request(with: urlImage, onCompletion: { _, error, _ in
            print("error \(String(describing: error))")
            
            if (String(describing:error) == "")
            {
                
                let imageResized = Toucan(image: imageTest.image!).resize(CGSize(width: self.imageProfile.frame.width, height: self.imageProfile.frame.height), fitMode: Toucan.Resize.FitMode.crop).image
                let imageCircle = Toucan(image: imageResized!).maskWithEllipse(borderWidth: 0.5, borderColor: UIColor.black).image
                self.imageProfile.image = imageCircle
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func closeMenu()
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.popViewController(animated: true)
        //_ = self.navigationController?.popToRootViewController(animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCellMenu
        
        let text = data[indexPath.row]
        cell.label.text = text
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true);
        print(indexPath.row)
        
        switch indexPath.row {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pushController = storyboard.instantiateViewController(withIdentifier: "HistoryTravelViewController") as! HistoryTravelViewController
            self.navigationController?.pushViewController(pushController, animated: true)
//        case 1:
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let pushController = storyboard.instantiateViewController(withIdentifier: "FavoritePlaces") as! FavoritePlaces
//            self.navigationController?.pushViewController(pushController, animated: true)
        case 1:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let pushController = storyboard.instantiateViewController(withIdentifier: "ConfigureAccountViewController") as! ConfigureAccountViewController
            self.navigationController?.pushViewController(pushController, animated: true)
        case 2:
            
            requestCloseSesion()
            
           
            
            //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
            //self.view.window?.rootViewController?.presentedViewController!.dismiss(animated: true, completion: nil)
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let pushController = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
//
//            let targetViewController = pushController // this is that controller, that you want to be embedded in navigation controller
//            let targetNavigationController = UINavigationController(rootViewController: targetViewController)
//
//            UIView.animate(withDuration: 0.3, animations: {
//                self.navigationController!.viewControllers.removeAll()
//            })
//
//            self.present(targetNavigationController, animated: true, completion: nil)
            
        default:
            print("error")
        }
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let pushController = storyboard.instantiateViewController(withIdentifier: "detalle") as! Detalle
//        self.navigationController?.pushViewController(pushController, animated: true)
//
        
    }
    
    func requestCloseSesion()
    {
        HUD.show(.progress)
       
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        let parameters: Parameters = [
            "id_usuario": id,
        ]
        
        print("WS_CANCEL_TRAVEL")
        print("\(ApiDefinition.WS_CIERRA_SESION)?id_usuario=\(id)")
        
        Alamofire.request(ApiDefinition.WS_CIERRA_SESION, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_CIERRA_SESION: \(json)")
                let resultado = json["resultado"]
                let res = resultado[0]["resultado"]
                if res != 0
                {
                    timer.invalidate()
                     HUD.flash(.success, delay: 0.5)
                    
                    UserDefaults.standard.setValue("false", forKey: DataPersistent.TERMINAL_LOGIN)
                    UserDefaults.standard.synchronize()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let mapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                    
                    mapViewController.resetTravel()
                    
                    let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
                    Messaging.messaging().unsubscribe(fromTopic: "c\(id)")
                    
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                        (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
                    }
      
                }
                else
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "Taxsi", message: "Ha ocurrido un error al cerrar sesión, por favor intente nuevamente", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print(error)
                 HUD.flash(.error, delay: 0.5)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
