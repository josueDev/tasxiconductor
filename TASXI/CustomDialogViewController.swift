//
//  TestViewController.swift
//  LSDialogViewController
//
//  Created by Daisuke Hasegawa on 2016/05/17.
//  Copyright © 2016年 Libra Studio, Inc. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import PKHUD

class CustomDialogViewController: UIViewController {
    
    var delegate: MapViewController?
    var idTravel : String = ""
    
    @IBOutlet var label: UILabel!
    @IBOutlet var image: UIImageView!
    @IBOutlet var star1: UIButton!
    @IBOutlet var star2: UIButton!
    @IBOutlet var star3: UIButton!
    @IBOutlet var star4: UIButton!
    @IBOutlet var star5: UIButton!
    @IBOutlet var textView: UITextView!
    
    @IBOutlet var labelNombre: UILabel!
    
    
    var punctuation = 3
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.defaultStar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
 

        UIView.animate(withDuration: 2.0, animations: {
            //self.defaultStar()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // adjust height and width of dialog
        self.view.bounds.size.height = UIScreen.main.bounds.size.height * 0.7
        self.view.bounds.size.width = UIScreen.main.bounds.size.width * 0.8
        
        label.adjustsFontSizeToFitWidth = true
        
        star1.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        star2.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        star3.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        star4.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        star5.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        star1.addTarget(self, action: #selector(self.pushStar1), for: UIControl.Event.touchUpInside)
        star2.addTarget(self, action: #selector(self.pushStar2), for: UIControl.Event.touchUpInside)
        star3.addTarget(self, action: #selector(self.pushStar3), for: UIControl.Event.touchUpInside)
        star4.addTarget(self, action: #selector(self.pushStar4), for: UIControl.Event.touchUpInside)
        star5.addTarget(self, action: #selector(self.pushStar5), for: UIControl.Event.touchUpInside)
        
        flagDialogEvaluateDriver = true
        
        let nombre_pasajero = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre_pasajero)!)
        labelNombre.adjustsFontSizeToFitWidth = true
        labelNombre.text = nombre_pasajero
        
        UIView.animate(withDuration: 2.0, animations: {
            //self.defaultStar()
        })
        
    }

    // close dialogView
    @IBAction func closeButton(_ sender: AnyObject) {
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        HUD.show(.progress)
        
        //let place = ObjectPendingTravel.getTravel().object(at: 0) as! ObjectPendingTravel
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        let id_viaje_pendiente = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje_pendiente)!)
        
        let parameters: Parameters = [
            "id_conductor": id,
            "id_viaje": id_viaje_pendiente,
            "valoracion": punctuation,
            "comentario": textView.text! as Any
        ]
        
        
        print("WS_EVALUA_PASAJERO")
        print("\(ApiDefinition.WS_EVALUA_PASAJERO)?id_conductor=\(id)&id_viaje=\(id_viaje_pendiente as Any)&valoracion=\(punctuation)&comentario=\(textView.text! as Any)")
        
        Alamofire.request(ApiDefinition.WS_EVALUA_PASAJERO, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                flagDialogEvaluateDriver = false
                let json = JSON(value)
                
                
                print("JSON: \(json)")
                let resultado = json["resultado"]
                print("resultado: \(resultado)")
                let res = resultado[0]["resultado"]
                if res != 0
                {
                   HUD.flash(.success, delay: 0.5)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.delegate?.dismissDialog()
                    })
                }
                else
                {
                    HUD.flash(.error, delay: 0.5)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.delegate?.dismissDialog()
                    })
                }
            case .failure(let error):
                print(error)
                flagDialogEvaluateDriver = false
                HUD.flash(.error, delay: 0.5)
                UIView.animate(withDuration: 0.5, animations: {
                    self.delegate?.dismissDialog()
                })
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        //self.delegate?.dismissDialog()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @objc func pushStar1() {
        punctuation = 1
        star1.imageView?.image = UIImage(named: "star_full.png")
        star2.imageView?.image = UIImage(named: "star_empty.png")
        star3.imageView?.image = UIImage(named: "star_empty.png")
        star4.imageView?.image = UIImage(named: "star_empty.png")
        star5.imageView?.image = UIImage(named: "star_empty.png")
    }
    
    @objc func pushStar2() {
        punctuation = 2
        star1.imageView?.image = UIImage(named: "star_full.png")
        star2.imageView?.image = UIImage(named: "star_full.png")
        star3.imageView?.image = UIImage(named: "star_empty.png")
        star4.imageView?.image = UIImage(named: "star_empty.png")
        star5.imageView?.image = UIImage(named: "star_empty.png")
    }
    
     @objc func pushStar3() {
        punctuation = 3
        star1.imageView?.image = UIImage(named: "star_full.png")
        star2.imageView?.image = UIImage(named: "star_full.png")
        star3.imageView?.image = UIImage(named: "star_full.png")
        star4.imageView?.image = UIImage(named: "star_empty.png")
        star5.imageView?.image = UIImage(named: "star_empty.png")
    }

    
    @objc func pushStar4() {
        punctuation = 4
        UIView.animate(withDuration: 0.5, animations: {
            self.star1.imageView?.image = UIImage(named: "star_full.png")
            self.star2.imageView?.image = UIImage(named: "star_full.png")
            self.star3.imageView?.image = UIImage(named: "star_full.png")
            self.star4.imageView?.image = UIImage(named: "star_full.png")
            self.star5.imageView?.image = UIImage(named: "star_empty.png")
        })
    }
    
    
     @objc func pushStar5() {
        punctuation = 5
        UIView.animate(withDuration: 0.5, animations: {
            self.star1.imageView?.image = UIImage(named: "star_full.png")
            self.star2.imageView?.image = UIImage(named: "star_full.png")
            self.star3.imageView?.image = UIImage(named: "star_full.png")
            self.star4.imageView?.image = UIImage(named: "star_full.png")
            self.star5.imageView?.image = UIImage(named: "star_full.png")
        })

    }
    
    func defaultStar() {
        punctuation = 3
        star1.imageView?.image = UIImage(named: "star_full.png")
        star2.imageView?.image = UIImage(named: "star_full.png")
        star3.imageView?.image = UIImage(named: "star_full.png")
        star4.imageView?.image = UIImage(named: "star_empty.png")
        star5.imageView?.image = UIImage(named: "star_empty.png")
    }
}
