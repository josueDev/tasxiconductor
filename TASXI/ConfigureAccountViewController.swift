//
//  ViewControllerConfigureAccount.swift
//  Taxsi
//
//  Created by Josué :D on 11/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD
import AAImagePicker
import Toucan

class ConfigureAccountViewController: UIViewController , UIScrollViewDelegate ,UITextFieldDelegate{

    @IBOutlet var imageProfile: UIImageView!
    
    @IBOutlet var name: UILabel!
    @IBOutlet var phone: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var fieldPassword2: UITextField!
    @IBOutlet var fieldNewPassword: UITextField!
    @IBOutlet var fieldRepeatNewPassword: UITextField!
    @IBOutlet var buttonUpdatePassword: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var editPhoto: UIButton!
    
    var imagePicker : AAImagePicker!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "CONFIGURACIÓN DE CUENTA"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        
        let urlImage = String(describing: UserDefaults.standard.value(forKey: DataPersistent.imageUrl)!)
        
        print("urlImage \(urlImage)")
        let imageTest = UIImageView()
        imageTest.load.request(with: urlImage, onCompletion: { _, error, _ in
            print("error \(String(describing: error))")
            if (String(describing: error) == "")
            {
                let imageResized = Toucan(image: imageTest.image!).resize(CGSize(width: self.imageProfile.frame.width, height: self.imageProfile.frame.height), fitMode: Toucan.Resize.FitMode.crop).image
                let imageCircle = Toucan(image: imageResized!).maskWithEllipse(borderWidth: 0, borderColor: UIColor.black).image
                self.imageProfile.image = imageCircle
            }
        })

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        imagePicker = AAImagePicker()
        
         //self.buttonUpdateData.layer.cornerRadius = CGFloat(5)
         self.buttonUpdatePassword.layer.cornerRadius = CGFloat(5)
        
//        fieldAge.inputView = UIView()
//        fieldAge.delegate = self
//
//        fieldGender.inputView = UIView()
//        fieldGender.delegate = self
        
        
        //buttonUpdateData.addTarget(self, action: #selector(self.updateData), for: UIControlEvents.touchUpInside)
        buttonUpdatePassword.addTarget(self, action: #selector(self.updatePassword), for: UIControl.Event.touchUpInside)
        editPhoto.addTarget(self, action: #selector(self.editPhotoMethod), for: UIControl.Event.touchUpInside)
        
        fillLabels()
        fillFields()
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func editPhotoMethod()
    {
        imagePicker.present { (image) in
            // Get your image
            print(image)
            let imageResized = Toucan(image: image).resize(CGSize(width: self.imageProfile.frame.width, height: self.imageProfile.frame.height), fitMode: Toucan.Resize.FitMode.crop).image
            let imageCircle = Toucan(image: imageResized!).maskWithEllipse(borderWidth: 0, borderColor: UIColor.black).image
            self.imageProfile.image = imageCircle
            self.imageProfile.contentMode = .scaleAspectFit
            
            URLCache.shared.removeAllCachedResponses()
            URLCache.shared.diskCapacity = 0
            URLCache.shared.memoryCapacity = 0
            
            if let cookies = HTTPCookieStorage.shared.cookies {
                for cookie in cookies {
                    HTTPCookieStorage.shared.deleteCookie(cookie)
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
        if scrollView.contentOffset.x<0 {
            scrollView.contentOffset.x = 0
        }
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == fieldAge {
//
//            var array = [String]()
//            for i in 18...100 {
//                array.append(String(i))
//            }
//
//            DPPickerManager.shared.showPicker(title: "Edad", selected: "18", strings: array) { (value, index, cancel) in
//                if !cancel {
//                    // TODO: you code here
//                    debugPrint(value as Any)
//                    self.fieldAge.text = value
//                }
//            }
//
//            return false;
//        }
//
//        if textField == fieldGender {
//
//            let values = ["Masculino", "Femenino"]
//            DPPickerManager.shared.showPicker(title: "Sexo", selected: "Masculino", strings: values) { (value, index, cancel) in
//                if !cancel {
//                    // TODO: you code here
//                    debugPrint(value as Any)
//                    self.fieldGender.text = value
//                }
//            }
//            return false;
//        }
//        return true
//    }
    
//    @objc func updateData()
//    {
//        if ((fieldName.text != "") && (fieldLastName.text != "")  && (fieldGender.text != "")  && (fieldAge.text != "")  && (fieldEmail.text != "")  && (fieldEmail.text != "")  && (fieldPhone.text != "") )
//        {
//            requestUpdatePassenger()
//        }
//        else
//        {
//            let alert = UIAlertController(title: "Taxsi", message: "Por favor no deje campos de datos vacíos", preferredStyle: .alert)
//            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
//            alert.addAction(actionOK)
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
    
    @objc func updatePassword()
    {
 
        if ((fieldPassword2.text != "") && (fieldNewPassword.text != "")  && (fieldRepeatNewPassword.text != "") )
        {
            if (fieldNewPassword.text == fieldRepeatNewPassword.text)
            {
                 requestUpdatePassword()
            }
            else
            {
                let alert = UIAlertController(title: "Taxsi", message: "Su nueva contraseña debe coincidir", preferredStyle: .alert)
                let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                alert.addAction(actionOK)
                self.present(alert, animated: true, completion: nil)
            }
           

        }
        else
        {
            let alert = UIAlertController(title: "Taxsi", message: "Por favor no deje campos de contraseña vacíos", preferredStyle: .alert)
            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
            alert.addAction(actionOK)
            self.present(alert, animated: true, completion: nil)
  
        }
    }
    
    
//    func requestUpdatePassenger()
//    {
//        HUD.show(.progress)
//
//
//        var gender = "M"
//        if fieldGender.text! == "Masculino"
//        {
//            gender = "H"
//        }
//
//        var imageString = ""
//        if self.imageProfile.image != nil
//        {
//            imageString = convertImageToBase64(image: self.imageProfile.image!)
//        }
//        //print(imageString)
//
//        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
//        let parameters: Parameters = [
//            "id_pasajero" : id,
//            "nombre": fieldName.text! as Any,
//            "apellidos": fieldLastName.text! as Any,
//            "telefono": fieldPhone.text! as Any,
//            "email": fieldEmail.text! as Any,
//            "sexo": gender,
//            "edad": fieldAge.text! as Any,
//            "foto": imageString
//        ]
//
//        //print(parameters)
//
//        Alamofire.request(ApiDefinition.WS_UPDATE_PASSENGER, method:  .post, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
//
//            switch response.result {
//
//            case .success(let value):
//                let json = JSON(value)
//                print("JSON: \(json)")
//                let pasajero = json["pasajero"]
//                let id = pasajero[0]["id"]
//                //let nombre: String = json["pasajero"]["nombre"].stringValue
//                //print("id " + id)
//                if id != 0
//                {
//                    HUD.flash(.success, delay: 0.5)
//
//                    let id = pasajero[0]["id"].string!
//                    let nombre = pasajero[0]["nombre"].string!
//                    let apellidos = pasajero[0]["apellidos"].string!
//                    let edad = pasajero[0]["edad"].string!
//                    let emailRes = pasajero[0]["email"].string!
//                    let sexo = pasajero[0]["sexo"].string!
//                    let telefono = pasajero[0]["telefono"].string!
//
//                    UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
//                    UserDefaults.standard.setValue(id, forKey: DataPersistent.id)
//                    UserDefaults.standard.setValue(nombre, forKey: DataPersistent.nombre)
//                    UserDefaults.standard.setValue(edad, forKey: DataPersistent.edad)
//                    UserDefaults.standard.setValue(apellidos, forKey: DataPersistent.apellidos)
//                    UserDefaults.standard.setValue(emailRes, forKey: DataPersistent.email)
//                    UserDefaults.standard.setValue(sexo, forKey: DataPersistent.sexo)
//                    UserDefaults.standard.setValue(telefono, forKey: DataPersistent.telefono)
//                    UserDefaults.standard.synchronize()
//
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                        self.fillLabels()
//                        self.fillFields()
//                    }
//                }
//                else
//                {
//                    HUD.flash(.error, delay: 0.5)
//                }
//
//            case .failure(let error):
//                print(error)
//                HUD.flash(.error, delay: 0.5)
//            }
//        }
//    }
    
    func requestUpdatePassword()
    {
        HUD.show(.progress)
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        let parameters: Parameters = [
            
            "id_pasajero" : id,
            "viejopass": fieldPassword2.text! as Any,
            "nuevopass": fieldNewPassword.text! as Any
            
        ]
        
        //print(parameters)
        print("\(ApiDefinition.WS_UPDATE_PASSWORD_CONDUCTOR)?id_pasajero=\(id)&viejopass=\(fieldPassword2.text! as Any)&nuevopass=\(fieldNewPassword.text! as Any)")
        
        Alamofire.request(ApiDefinition.WS_UPDATE_PASSWORD_CONDUCTOR, method:  .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            
            switch response.result {
                
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                let resultado = json["resultado"]
                print(resultado)
                let res = resultado[0]["resultado"]
                if (res == 0)
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.alertErrorServer()
                    }
                }
                else if(res == 1)
                {
   
           
                    
                    HUD.flash(.success, delay: 0.5)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                        self.fieldPassword2.text = ""
                        self.fieldNewPassword.text = ""
                        self.fieldRepeatNewPassword.text = ""
                        
                        let alert = UIAlertController(title: "Taxsi", message: "Contraseña actualizada correctamente", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        
                        let alert = UIAlertController(title: "Taxsi", message: "Su contraseña actual no coincide, por favor verifíquela", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
            case .failure(let error):
                print(error)
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    self.alertErrorServer()
                }
            }
        }
    }
    
    
    func alertErrorServer()
    {
        let alert = UIAlertController(title: "Taxsi", message: "Ha ocurrido un error, por favor intente nuevamente", preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    
    func fillLabels()
    {
        let uName = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        let uPhone = String(describing: UserDefaults.standard.value(forKey: DataPersistent.telefono)!)
        let uLastName = String(describing: UserDefaults.standard.value(forKey: DataPersistent.apellidos)!)
        let uEmail = String(describing: UserDefaults.standard.value(forKey: DataPersistent.email)!)
        
        name.text = "\(uName) \(uLastName)"
        phone.text = uPhone
        email.text = uEmail
    }
    
    func fillFields()
    {
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.nombre)!)
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.apellidos)!)
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.telefono)!)
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.email)!)
        let uGender = String(describing: UserDefaults.standard.value(forKey: DataPersistent.sexo)!)
        _ = String(describing: UserDefaults.standard.value(forKey: DataPersistent.edad)!)
        
        
        var stringGender = "Masculino"
        if (uGender != "H")
        {
            stringGender = "Femenino"
        }

//        fieldName.text = uName
//        fieldLastName.text = uLastName
//        fieldGender.text = stringGender
//        fieldAge.text = uAge
//        fieldEmail.text = uEmail
//        fieldPhone.text = uPhone
    }
    
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
    return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    


}
