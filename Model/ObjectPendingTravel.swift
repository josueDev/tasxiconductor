//
//  ObjectPendingTravel.swift
//  TASXI
//
//  Created by Josué :D on 30/06/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

private var array: NSMutableArray = NSMutableArray()

class ObjectPendingTravel: NSObject {
    var id: String
    var name : String
    var address : String
    var city : String
    var latitude : CLLocationDegrees
    var longitude : CLLocationDegrees
    var image : String
    var distance : String
    
    
    init( id : String,name : String, address : String, city : String, latitude : CLLocationDegrees, longitude : CLLocationDegrees,image : String, distance : String) {
        
        self.id = id
        self.name = name
        self.address = address
        self.city = city
        self.latitude = latitude
        self.longitude = longitude
        self.image = image
        self.distance = distance
        
    }
    class func getTravel() -> NSMutableArray {
        return array
    }
    
    class func setTravel( listac : NSMutableArray ) {
        array = listac
    }
}
