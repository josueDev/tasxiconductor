//
//  Definitions.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class ApiDefinition: NSObject {

    //static let API_SERVER = "http://sinergiadigital.com.mx"
    static let API_SERVER = "https://taxsi.mx"
    static let WS_LOGIN = API_SERVER + "/servicios/autenticaPasajero.php"
    static let WS_REQUEST_TRAVEL = API_SERVER +  "/servicios/solicitaViaje.php"
    static let WS_REQUEST_TRAVEL_2 = API_SERVER +  "/servicios/solicitaViaje2.php"
    static let WS_STATUS_TRAVEL = API_SERVER +  "/servicios/getEstatusPasajero.php"
    static let WS_EVALUATE_DRIVER = API_SERVER +  "/servicios/evaluaConductor.php"
    static let WS_PENDING_EVALUATE_DRIVER = API_SERVER +  "/servicios/pasajeroTieneCalifPendiente.php"
    static let WS_UPDATE_PASSENGER = API_SERVER +  "/servicios/updatePasajero.php"
    static let WS_UPDATE_PASSWORD = API_SERVER +  "/servicios/updatePasswordPasajero.php"
    static let WS_DATA_TRAVEL = API_SERVER + "/servicios/getDatosViajePasajero.php"
    static let WS_CANCEL_TRAVEL = API_SERVER + "/servicios/cancelaViaje.php"
    static let WS_GET_COOR_DRIVER = API_SERVER + "/servicios/getCoordenadasViaje.php"
    static let WS_GET_HISTORY_TRAVEL = API_SERVER + "/servicios/getHistoricoViajesPasajero.php"
    static let WS_CALCULATE_PRICE = API_SERVER + "/servicios/calcularTarifa.php"
    static let WS_ADD_FAVORITE = API_SERVER + "/servicios/agregaLugar.php"
    static let WS_ERASE_FAVORITE = API_SERVER + "/servicios/eliminaLugar.php"
    static let WS_GET_FAVORITES = API_SERVER + "/servicios/getLugaresPasajero.php"
    static let WS_CANCEL_TRAVELS = API_SERVER + "/servicios/cancelaViajesPendientesPasajero.php"
    static let WS_TEL = API_SERVER + "/servicios/getTelefonoCentral.php"


    static let WS_AUTENTICA_CONDUCTOR = API_SERVER + "/servicios/autenticaConductor.php"
    static let WS_TOMA_VIAJE = API_SERVER +  "/servicios/tomaViaje.php"
    static let WS_INICIA_VIAJE = API_SERVER +  "/servicios/iniciaViaje.php"
    static let WS_FINALIZA_VIAJE = API_SERVER +  "/servicios/finalizaViaje.php"
    static let WS_EVALUA_PASAJERO = API_SERVER +  "/servicios/evaluaPasajero.php"
    static let WS_GET_DATOS_VIAJE_CONDUCTOR = API_SERVER +  "/servicios/getDatosViajeConductor.php"
    static let WS_CANCELA_VIAJE_CONDUCTOR = API_SERVER +  "/servicios/cancelaViajeConductor.php"
    static let WS_ACTUALIZAR_COORDENADAS_CHOFER = API_SERVER +  "/servicios/actualizaCoordenadasChofer.php"
    static let WS_HISTORICO_VIAJES_CONDUCTOR = API_SERVER + "/servicios/getHistoricoViajesConductor.php"
    static let WS_ESTATUS_CONDUCTOR = API_SERVER + "/servicios/getEstatusConductor.php"
    static let WS_CONDUCTOR_TIENE_CALIFICACION_PENDIENTE = API_SERVER + "/servicios/conductorTieneCalifPendiente.php"
    static let WS_ENVIAR_MENSAJE_AUTOMATICO = API_SERVER + "/servicios/enviarMensajeAutomatico.php"
    static let WS_UPDATE_PASSWORD_CONDUCTOR = API_SERVER + "/servicios/updatePasswordConductor.php"
    static let WS_CALCULAR_TARIFA = API_SERVER + "/servicios/calcularTarifa.php"
    static let WS_GET_TELEFONO_CENTRAL = API_SERVER + "/servicios/getTelefonoCentral.php"
    static let WS_ALERTA_PASAJE = API_SERVER + "/servicios/alertaPasaje.php"
    static let WS_CIERRA_SESION = API_SERVER + "/servicios/cierraSesion.php"
   
   
}

/*
public interface TaxsiAppDefinitions {
    @GET("/servicios/autenticaConductor.php")
    Call<ResponseBody> login(@Query("email")String email, @Query("password")String password);

    @GET("/servicios/tomaViaje.php")
    Call<ResponseBody> takeTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/iniciaViaje.php")
    Call<ResponseBody> initTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/finalizaViaje.php")
    Call<ResponseBody> endTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje,@Query("latitud_llegada_real")String latitud_llegada_real, @Query("longitud_llegada_real")String longitud_llegada_real);

    @GET("/servicios/evaluaPasajero.php")
    Call<ResponseBody> evaluatePassenger(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje,@Query("valoracion")String valoracion, @Query("comentario")String comentario);

    @GET("/servicios/getDatosViajeConductor.php")
    Call<ResponseBody> getDataTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/cancelaViajeConductor.php")
    Call<ResponseBody> cancelTrip(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/actualizaCoordenadasChofer.php")
    Call<ResponseBody> sendLocationDriver(@Query("id_conductor")String id_conductor, @Query("latitud")String latitud, @Query("longitud")String longitud);

    @GET("/servicios/getHistoricoViajesConductor.php")
    Call<ResponseBody> getHistory(@Query("id_conductor")String id_conductor, @Query("id_viaje")String id_viaje);

    @GET("/servicios/getEstatusConductor.php")
    Call<ResponseBody> getStatusDriver(@Query("id_conductor")String id_conductor);

    @GET("/servicios/conductorTieneCalifPendiente.php")
    Call<ResponseBody> getCalificacionPendiente(@Query("id_conductor")String id_conductor);

    @GET("/servicios/enviarMensajeAutomatico.php")
    Call<ResponseBody> sendNotification(@Query("id_conductor")String id_conductor, @Query("msj")String msj);

    @GET("/servicios/updatePasswordConductor.php")
    Call<ResponseBody> changePassword(@Query("id_conductor")String id_conductor, @Query("viejopass")String viejoPass, @Query("nuevopass")String nuevopass);

    @GET("/servicios/calcularTarifa.php")
    Call<ResponseBody> calcularTarifa(@Query("distancia")String id_conductor, @Query("tiempo")String viejoPass);

    @GET("/servicios/getTelefonoCentral.php")
    Call<ResponseBody> getTelefonosCentral();

    @GET("/servicios/alertaPasaje.php")
    Call<ResponseBody> getAlertNearTravel(@Query("latitud") String latitud , @Query("longitud") String longitud);


}
*/
