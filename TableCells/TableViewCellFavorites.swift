//
//  TableViewCellFavorites.swift
//  TASXI
//
//  Created by Josue Hernandez on 9/14/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

class TableViewCellFavorites: UITableViewCell {
    
    
    @IBOutlet var labelText: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
